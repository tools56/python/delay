"""
   delay.py
   Calcul de delais ecouler entre 2 TIMESTAMP
   :author: Stephane Hamel
"""
import sys
from datetime import datetime

FMT = '%Y-%m-%d %H:%M:%S,%f'
tstamp_deb = datetime.strptime(sys.argv[1], FMT)
tstamp_fin = datetime.strptime(sys.argv[2], FMT)

if tstamp_deb > tstamp_fin:
    DELAY_RETURN=999
else:
    td = tstamp_fin - tstamp_deb
    DELAY_RETURN=td.total_seconds()+0

print ("%s"% DELAY_RETURN)
